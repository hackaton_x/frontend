/// <reference types="cypress" />
const frontendUrl = 'http://localhost:8081/'

describe('Book a trip', () => {

  it('Find trip', () => {
    cy.visit(`${frontendUrl}`);
    cy.get('#departure').type('Paris');
    cy.get('#arrival').type('Bruxelles');
    cy.get('#date')
      .type('2024-04-19')
      .trigger('change');
    cy.get('#search').click();

    cy.get('#6622377a4c6d311425bdb13c').click();
    cy.get('#purchase').click();
  })
})


